package refrigerator;
import java.sql.Ref;


public class Main  {
	
	public static void main(String args[]) {
		Refrigerator ref = new Refrigerator();
		try {
			ref.put("Apple");
			ref.put("banana");
			ref.put("Tuna");
			ref.put("Milk");
			ref.put("Jam");
			System.out.println("-----STUFF IN REFRIGERATOR----(before takeout)\n"+ref.toString());
			ref.put("Chocolate");
			
		} catch (FullException e) {
			System.err.println("Error : "+e.getMessage());
		
		}
		System.out.println("------TAKE OUT------");
		System.out.println("Take thing out of the refrigerator: "+ ref.takeOut("Apple"));
		System.out.println("Take thing out of the refrigerator: "+ ref.takeOut("Cheese"));
		
		System.out.println("\n------STUFF IN REFRIGERATOR-----(after takeout)");
		System.out.println(ref.toString());
		
		
	}
	
	
	
	

}
