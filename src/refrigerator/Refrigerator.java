package refrigerator;
import java.util.ArrayList;


public class Refrigerator {
	
	private int size;
	private ArrayList<String> things;
	
	public Refrigerator(){
		this.size = 5;
		things = new ArrayList<String>();

	}
	
	public void put(String stuff)throws FullException{
		if(things.size()<size){
			things.add(stuff);
		}
		else {
			throw new FullException("Your refrigetator are full please take out somethings.");
			
		}
		
	}
	
	public String takeOut(String stuff){
		String take = "";
		
		if(things.contains(stuff)){
			take = stuff;
			things.remove(stuff);
			
		}
		else {
			take = null;
		}
		return take;
	}
	
	public String toString(){
		String listStr = "";
		for(String t : things){
			listStr += "stuff in refrigerator : "+t+"\n";
		}
		return listStr;
	}
	

}
