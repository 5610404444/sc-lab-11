package testexception;

public class Main {
	
	public static void main(String args[])throws DataException,FormatException{
		try{
			Myclass c = new Myclass(1,200);
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		}catch(DataException e){
			System.out.print("D");
		}catch(FormatException e){
			System.out.print("E");
			e.printStackTrace(); //unchecked exception
		}finally{
			System.out.print("F");
		}
		System.out.print("G");
		
	}

}
