package testexception;

public class Myclass {
	private int numberofclass; 
	private long id;
	public Myclass(int num,int id){
		this.numberofclass = num;
		this.id = id;
	}
	public void methX() throws DataException{
		if(numberofclass < 0){
			throw new DataException();//check
		}
		
		
	}
	public void methY() throws FormatException {
		if(id<561040000){
			throw new FormatException("id must be integer more than 561040000 ");//Unchecked 
		}
		
	}

}
