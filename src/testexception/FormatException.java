package testexception;

public class FormatException extends IllegalArgumentException {
	
	public FormatException(){
		super();
	}
	
	public FormatException(String message){
		super(message);
	}

}
