package wordcounter;
import java.util.HashMap;


public class WordCounter {
	private String message;
	private HashMap<String, Integer> wordCount;
	
	public WordCounter(String aMessage){
		message = aMessage;
		wordCount = new HashMap<String, Integer>();
		
	}
	
	public void Count(){
		String[] splitword = message.split(" ");
		for (String m : splitword){
			
			if(wordCount.containsKey(m)){
				wordCount.put(m, 1);
				wordCount.put(m, wordCount.get(m)+1);
			}
			else{
				wordCount.put(m, 1);
			}
		}
	}
	
	public int hasWord(String word){
		int num = 0;
		if(wordCount.containsKey(word)){
			num = wordCount.get(word);
		}
		return num;
	}

}
